
var exec = require('cordova/exec');

var PLUGIN_NAME = 'CordovaPluginAffectiva';

var CordovaPluginAffectiva = {
  startDetector: function(cb) {
    exec(cb, null, PLUGIN_NAME, 'startDetector', []);
  },
  stopDetector: function(cb) {
    exec(cb, null, PLUGIN_NAME, 'stopDetector', []);
  }
};

module.exports = CordovaPluginAffectiva;