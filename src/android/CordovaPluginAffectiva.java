/**
 */
package com.budsoft.cordova;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import org.json.JSONArray;
import org.json.JSONException;
import android.content.Intent;
import android.view.SurfaceHolder;
import android.content.Context;

public class CordovaPluginAffectiva extends CordovaPlugin implements SurfaceHolder.Callback {
  private static CallbackContext cbc;

  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);
    AffectivaSurfaceActivity.cordova = cordova;
  }

  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    AffectivaSurfaceActivity.cbc = callbackContext;
    Context context = this.cordova.getActivity().getApplicationContext();
    Intent intent = new Intent(context, AffectivaSurfaceActivity.class);
    this.cordova.startActivityForResult(this, intent, 1);
    return true;
  }

  @Override
  public void surfaceCreated(SurfaceHolder holder) {
    // TODO Auto-generated method stub
  }

  @Override
  public void surfaceChanged(final SurfaceHolder holder, int format, int width, int height) {
// TODO Auto-generated method stub
  }

  @Override
  public void surfaceDestroyed(SurfaceHolder holder) {
    // TODO Auto-generated method stub
  }

}
