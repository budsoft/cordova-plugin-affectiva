/**
 * Created by Gilad on 6/7/2017.
 */
package com.budsoft.cordova;

import android.Manifest;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.affectiva.android.affdex.sdk.Frame;
import com.affectiva.android.affdex.sdk.detector.CameraDetector;
import com.affectiva.android.affdex.sdk.detector.Detector;
import com.affectiva.android.affdex.sdk.detector.Face;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.util.List;



/**
 * This is a very bare sample app to demonstrate the usage of the CameraDetector object from Affectiva.
 * It displays statistics on frames per second, percentage of time a face was detected, and the user's smile score.
 *
 * The app shows off the maneuverability of the SDK by allowing the user to start and stop the SDK and also hide the camera SurfaceView.
 *
 * For use with SDK 2.02
 */
public class AffectivaSurfaceActivity extends Activity implements Detector.ImageListener, CameraDetector.CameraEventListener, SurfaceHolder.Callback {

  public static CallbackContext cbc;
  public static CordovaInterface cordova;
  public static CordovaWebView webView;

  SurfaceView cameraPreview;

  boolean isSDKStarted = false;

  int containerViewId = 1;
  FrameLayout mainLayout;

  CameraDetector detector;

  int previewWidth = 0;
  int previewHeight = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

//    mainLayout = (FrameLayout) new FrameLayout(cordova.getActivity());

    FrameLayout containerView = (FrameLayout)cordova.getActivity().findViewById(containerViewId);
    containerView = new FrameLayout(cordova.getActivity().getApplicationContext());
    containerView.setId(containerViewId);
    FrameLayout.LayoutParams containerLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
    cordova.getActivity().addContentView(containerView, containerLayoutParams);

    cameraPreview = new SurfaceView(this) {
      @Override
      public void onMeasure(int widthSpec, int heightSpec) {
        setMeasuredDimension(300,300);
      }
    };

//    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
    FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
    cameraPreview.setLayoutParams(frameParams);

//    cameraPreview.setAlpha(0);
    containerView.addView(cameraPreview,0);
//    setContentView(containerView);
    webView.getView().bringToFront();
    webView.getView().requestFocus();

    detector = new CameraDetector(cordova.getActivity().getApplicationContext(), CameraDetector.CameraType.CAMERA_FRONT, cameraPreview);
    detector.setDetectAllAppearances(true);
    detector.setDetectAllEmojis(true);
    detector.setDetectAllEmotions(true);
    detector.setDetectAllExpressions(true);
    detector.setImageListener(this);
    detector.setOnCameraEventListener(this);
    isSDKStarted = true;
  }

  @Override
  public void onImageResults(List<Face> faces, Frame receievedFrame, float v) {
    final List<Face> list = faces;
    final Frame frame = receievedFrame;
    cordova.getThreadPool().execute(new Runnable() {
      public void run() {
        try {

          if (list == null || list.size() == 0){
//            PluginResult progressResult = new PluginResult(PluginResult.Status.OK, "NO FACE");
//            progressResult.setKeepCallback(true);
//            cbc.sendPluginResult(progressResult);
            return;
          }
          else {
            //HAS FACES RESULTS, PARSE TO JSON AND "callback.sendPluginResult"
            JSONObject jsonObj = new JSONObject();
            JSONObject jsonObjQualities = new JSONObject();
            JSONObject jsonObjAppearance = new JSONObject();
            JSONObject jsonObjEmojis = new JSONObject();
            JSONObject jsonObjExpressions = new JSONObject();
            JSONObject jsonObjEmotions = new JSONObject();
            JSONObject jsonObjMeasurements = new JSONObject();
            JSONObject jsonObjFrame = new JSONObject();

            try {
              //appearance
              jsonObjAppearance.put("Age", list.get(0).appearance.getAge().toString());
              jsonObjAppearance.put("Ethnicity", list.get(0).appearance.getEthnicity().toString());
              jsonObjAppearance.put("Gender", list.get(0).appearance.getGender().toString());
              jsonObjAppearance.put("Glasses", list.get(0).appearance.getGlasses().toString());

              //qualities
              jsonObjQualities.put("Brightness", String.valueOf(list.get(0).qualities.getBrightness()));

              //emojis
              jsonObjEmojis.put("Disappointed", String.valueOf(list.get(0).emojis.getDisappointed()));
              jsonObjEmojis.put("Dominant", String.valueOf(list.get(0).emojis.getDominant()));
              jsonObjEmojis.put("DominantEmoji", String.valueOf(list.get(0).emojis.getDominantEmoji()));
              jsonObjEmojis.put("Flushed", String.valueOf(list.get(0).emojis.getFlushed()));
              jsonObjEmojis.put("getKissing", String.valueOf(list.get(0).emojis.getKissing()));
              jsonObjEmojis.put("Laughing", String.valueOf(list.get(0).emojis.getLaughing()));
              jsonObjEmojis.put("Rage", String.valueOf(list.get(0).emojis.getRage()));
              jsonObjEmojis.put("Relaxed", String.valueOf(list.get(0).emojis.getRelaxed()));
              jsonObjEmojis.put("Scream", String.valueOf(list.get(0).emojis.getScream()));
              jsonObjEmojis.put("Smiley", String.valueOf(list.get(0).emojis.getSmiley()));
              jsonObjEmojis.put("Smirk", String.valueOf(list.get(0).emojis.getSmirk()));
              jsonObjEmojis.put("StuckOutTongue", String.valueOf(list.get(0).emojis.getStuckOutTongue()));
              jsonObjEmojis.put("StuckOutTongueWinkingEye", String.valueOf(list.get(0).emojis.getStuckOutTongueWinkingEye()));
              jsonObjEmojis.put("Wink", String.valueOf(list.get(0).emojis.getWink()));

              //expressions
              jsonObjExpressions.put("Attention", String.valueOf(list.get(0).expressions.getAttention()));
              jsonObjExpressions.put("Smirk", String.valueOf(list.get(0).expressions.getSmirk()));
              jsonObjExpressions.put("BrowFurrow", String.valueOf(list.get(0).expressions.getBrowFurrow()));
              jsonObjExpressions.put("BrowRaise", String.valueOf(list.get(0).expressions.getBrowRaise()));
              jsonObjExpressions.put("CheekRaise", String.valueOf(list.get(0).expressions.getCheekRaise()));
              jsonObjExpressions.put("ChinRaise", String.valueOf(list.get(0).expressions.getChinRaise()));
              jsonObjExpressions.put("Dimpler", String.valueOf(list.get(0).expressions.getDimpler()));
              jsonObjExpressions.put("EyeClosure", String.valueOf(list.get(0).expressions.getEyeClosure()));
              jsonObjExpressions.put("EyeWiden", String.valueOf(list.get(0).expressions.getEyeWiden()));
              jsonObjExpressions.put("InnerBrowRaise", String.valueOf(list.get(0).expressions.getInnerBrowRaise()));
              jsonObjExpressions.put("JawDrop", String.valueOf(list.get(0).expressions.getJawDrop()));
              jsonObjExpressions.put("LidTighten", String.valueOf(list.get(0).expressions.getLidTighten()));
              jsonObjExpressions.put("LipCornerDepressor", String.valueOf(list.get(0).expressions.getLipCornerDepressor()));
              jsonObjExpressions.put("LipPress", String.valueOf(list.get(0).expressions.getLipPress()));
              jsonObjExpressions.put("LipCornerDepressor", String.valueOf(list.get(0).expressions.getLipCornerDepressor()));
              jsonObjExpressions.put("LidTighten", String.valueOf(list.get(0).expressions.getLidTighten()));
              jsonObjExpressions.put("LipPucker", String.valueOf(list.get(0).expressions.getLipPucker()));
              jsonObjExpressions.put("LipStretch", String.valueOf(list.get(0).expressions.getLipStretch()));
              jsonObjExpressions.put("LipSuck", String.valueOf(list.get(0).expressions.getLipSuck()));
              jsonObjExpressions.put("NoseWrinkle", String.valueOf(list.get(0).expressions.getNoseWrinkle()));
              jsonObjExpressions.put("Smile", String.valueOf(list.get(0).expressions.getSmile()));
              jsonObjExpressions.put("Smirk", String.valueOf(list.get(0).expressions.getSmirk()));
              jsonObjExpressions.put("UpperLipRaise", String.valueOf(list.get(0).expressions.getUpperLipRaise()));
              jsonObjExpressions.put("Smile", String.valueOf(list.get(0).expressions.getSmile()));

              //emotions
              jsonObjEmotions.put("Anger", String.valueOf(list.get(0).emotions.getAnger()));
              jsonObjEmotions.put("Contempt", String.valueOf(list.get(0).emotions.getContempt()));
              jsonObjEmotions.put("Disgust", String.valueOf(list.get(0).emotions.getDisgust()));
              jsonObjEmotions.put("Engagement", String.valueOf(list.get(0).emotions.getEngagement()));
              jsonObjEmotions.put("Fear", String.valueOf(list.get(0).emotions.getFear()));
              jsonObjEmotions.put("Joy", String.valueOf(list.get(0).emotions.getJoy()));
              jsonObjEmotions.put("Sadness", String.valueOf(list.get(0).emotions.getSadness()));
              jsonObjEmotions.put("Surprise", String.valueOf(list.get(0).emotions.getSurprise()));
              jsonObjEmotions.put("Valence", String.valueOf(list.get(0).emotions.getValence()));

              //measurements
              jsonObjMeasurements.put("InterocularDistance", String.valueOf(list.get(0).measurements.getInterocularDistance()));

              //frame
              jsonObjFrame.put("ColorFormat", frame.getColorFormat().toString());
              jsonObjFrame.put("Height", frame.getHeight());
              jsonObjFrame.put("PixelCount", frame.getPixelCount());
              jsonObjFrame.put("TargetRotation", frame.getTargetRotation().toString());
              jsonObjFrame.put("Width", frame.getWidth());
              jsonObjFrame.put("Height", frame.getHeight());

              //global
              jsonObj.put("appearance", jsonObjAppearance);
              jsonObj.put("qualities", jsonObjQualities);
              jsonObj.put("emojis", jsonObjEmojis);
              jsonObj.put("expressions", jsonObjExpressions);
              jsonObj.put("emotions", jsonObjEmotions);
              jsonObj.put("measurements", jsonObjMeasurements);
              jsonObj.put("facePoints", list.get(0).getFacePoints());
              jsonObj.put("frame", jsonObjFrame);
              jsonObj.put("id", list.get(0).getId());

            } catch (JSONException e) {
              e.printStackTrace();
            }

            PluginResult progressResult = new PluginResult(PluginResult.Status.OK, jsonObj);
            progressResult.setKeepCallback(true);
            cbc.sendPluginResult(progressResult);

          }
        } catch (Throwable e) {
          cbc.error(e.toString());
        }
      }
    });
  }

  @SuppressWarnings("SuspiciousNameCombination")
  @Override
  public void onCameraSizeSelected(int width, int height, Frame.ROTATE rotate) {
    /*if (rotate == Frame.ROTATE.BY_90_CCW || rotate == Frame.ROTATE.BY_90_CW) {
      previewWidth = height;
      previewHeight = width;
    } else {
      previewHeight = height;
      previewWidth = width;
    }
    cameraPreview.requestLayout();*/
    cordova.getActivity().runOnUiThread(new Runnable() {

      public void run() {
        cameraPreview.requestLayout();
      }
    });

  }

  @Override
  protected void onResume() {
    super.onResume();
    if (isSDKStarted) {
      startDetector();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    stopDetector();
  }

  @Override
  public void surfaceCreated(SurfaceHolder holder) {
    // TODO Auto-generated method stub
  }

  @Override
  public void surfaceChanged(final SurfaceHolder holder, int format, int width, int height) {
// TODO Auto-generated method stub
  }

  @Override
  public void surfaceDestroyed(SurfaceHolder holder) {
    // TODO Auto-generated method stub
  }

  void startDetector() {
    if (!detector.isRunning()) {
      cordova.getActivity().runOnUiThread(new Runnable() {

        public void run() {
          detector.start();
        }
      });

    }
  }

  void stopDetector() {
    if (detector.isRunning()) {
      cordova.getActivity().runOnUiThread(new Runnable() {

        public void run() {
          detector.stop();
        }
      });

    }
  }

  void switchCamera(CameraDetector.CameraType type) {
    detector.setCameraType(type);
  }

}
